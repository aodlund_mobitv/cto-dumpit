#include "wifi_util.h"

#include <stdio.h>
#include <stdlib.h>
#include <pcap.h>

struct wifi_ctx {
    pcap_t * handle;
    char * iface;
};

int switch_channel(int channel) {
    char *cmd;
    int ret;

    ret = asprintf(&cmd, "/system/bin/wl channel %d", channel);
    if (ret > 0) {
        ret = system(cmd);
        fprintf(stderr, "%s => %d\n", cmd, ret);
        free(cmd);
    } 
    return ret;
}

int switch_monitor(int on) {
    char *cmd;
    int ret;

    ret = asprintf(&cmd, "/system/bin/wpa_cli driver monitor %d", on);
    if (ret > 0) {
        ret = system(cmd);
        fprintf(stderr, "%s => %d\n", cmd, ret);
        free(cmd);
    } 
    return ret;
}

wifi_ctx_t * init_wifi_context() {
    char reply_buf[PCAP_ERRBUF_SIZE];	        /* Error string */
    int ret;
    size_t len;
    
    wifi_ctx_t * ctx = (wifi_ctx_t*)calloc(1, sizeof(wifi_ctx_t));
    if (ctx) {
        ctx->iface = pcap_lookupdev(reply_buf);
        if (ctx->iface != NULL) {
            fprintf(stderr, "Device: %s\n", ctx->iface);
        } else {
            fprintf(stderr, "Couldn't find default device: %s\n", reply_buf);
            free(ctx);
            ctx = NULL;
        }
    } else {
        fprintf(stderr, "Couldn't allocate memory for wifi context\n");
    }

    return ctx;
}

void deinit_wifi_context(wifi_ctx_t * ctx) {
    free(ctx);
}

int start_wifi_capture(wifi_ctx_t * ctx) {
    char reply_buf[PCAP_ERRBUF_SIZE];	        /* Error string */
    int ret;
    size_t len;

    ctx->handle = pcap_open_live(ctx->iface, BUFSIZ, 1, 100, reply_buf);        // ..., snaplen, promisc, timeout, ... 
    if (ctx->handle != NULL) {
        ret = pcap_setnonblock(ctx->handle, 1, reply_buf);
        if (ret == -1) {
            fprintf(stderr, "Failed to set non blocking mode: %s\n", reply_buf);
            pcap_close(ctx->handle);
            return -1;
        }
    } else {
        fprintf(stderr, "Couldn't create pcap handle: %s\n", reply_buf);
        return -1;
    }   
    return 0;
}

void stop_wifi_capture(wifi_ctx_t * ctx) {
    pcap_close(ctx->handle);
    ctx->handle = NULL;
}

const void * next_wifi_packet(wifi_ctx_t * ctx, int *len) {
    struct pcap_pkthdr header;	            
    const void *ptr = pcap_next(ctx->handle, &header);
    *len = header.len;
    return ptr;
    
}
