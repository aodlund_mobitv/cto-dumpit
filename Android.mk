LOCAL_PATH:= $(call my-dir)

include $(CLEAR_VARS)

LOCAL_SRC_FILES := \
	dumpit.c \
	scan.c \
	wifi_util.c

LOCAL_MODULE:= dumpit

LOCAL_C_INCLUDES := \
	external/libpcap \

LOCAL_STATIC_LIBRARIES := \
	 libpcap

LOCAL_SHARED_LIBRARIES := \
	liblog \
		
include $(BUILD_EXECUTABLE)
