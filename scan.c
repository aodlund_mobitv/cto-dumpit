#include "scan.h"

#include <stdio.h>

const char * frame_type(wlan_header_t *header) {
    switch(header->frame_control.type) {
        case FRAME_TYPE_MANAGEMENT:
            return "Management";
        case FRAME_TYPE_CONTROL:
            return "Control";
        case FRAME_TYPE_DATA:
            return "Data";
    }
    return "Unknown";
}


uint8_t * get_da(wlan_header_t *header) {
    if (header->frame_control.to_ds) {
        return header->addr3;
    } else {
        return header->addr1;
    }
}

uint8_t * get_sa(wlan_header_t *header) {
    if (header->frame_control.from_ds) {
        return header->frame_control.to_ds ? header->addr4 : header->addr3;
    } else {
        return header->addr2;
    }
}

uint8_t * get_ra(wlan_header_t *header) {
    return header->addr1;
}

uint8_t * get_ta(wlan_header_t *header) {
    return header->addr2;
}

uint8_t * get_bssid(wlan_header_t *header) {
    if (header->frame_control.to_ds) {
        return header->frame_control.from_ds ? NULL : header->addr1;
    } else {
        return header->frame_control.from_ds ? header->addr2 : header->addr3;
    }
}

int is_data_packet(wlan_header_t *header) {
    return header->frame_control.type == FRAME_TYPE_DATA && 
          (header->frame_control.sub_type == FRAME_SUB_TYPE_DATA_DATA || header->frame_control.sub_type == FRAME_SUB_TYPE_DATA_QOS);
}


int is_same_transmitter(wlan_header_t *hdr1, wlan_header_t *hdr2) {

    uint8_t *ta1 = get_ta(hdr1);
    uint8_t *ta2 = get_ta(hdr2);
    
    return (ta1[0] == ta2[0]) &&
           (ta1[1] == ta2[1]) &&
           (ta1[2] == ta2[2]) &&
           (ta1[3] == ta2[3]) &&
           (ta1[4] == ta2[4]) &&
           (ta1[5] == ta2[5]);
}

// TODO: this should probably check the DA instead of the RA.
// Not changing it for now since we only listen to packets from the AP, 
// and in that case, RA and DA are the same
int is_broadcast(wlan_header_t *header) {
    uint8_t *ra = get_ra(header);

    return ra[0] == 0xFF && 
           ra[1] == 0xFF && 
           ra[2] == 0xFF && 
           ra[3] == 0xFF && 
           ra[4] == 0xFF && 
           ra[5] == 0xFF;

}

int is_from_ap(wlan_header_t *header) {
    return (header->frame_control.to_ds == 0) && (header->frame_control.from_ds == 1);
}


void print_addresses(wlan_header_t *header)
{

    uint8_t *da = get_da(header);    
    uint8_t *sa = get_sa(header);
    uint8_t *ra = get_ra(header);
    uint8_t *bssid = get_bssid(header);
    
    printf("Destination:   %02x:%02x:%02x:%02x:%02x:%02x\n", da[0], da[1], da[2], da[3], da[4], da[5]);
    printf("Source:        %02x:%02x:%02x:%02x:%02x:%02x\n", sa[0], sa[1], sa[2], sa[3], sa[4], sa[5]);
    printf("Receiver:      %02x:%02x:%02x:%02x:%02x:%02x\n", ra[0], ra[1], ra[2], ra[3], ra[4], ra[5]);
    if (bssid) printf("BSSID:         %02x:%02x:%02x:%02x:%02x:%02x\n", bssid[0], bssid[1], bssid[2], bssid[3], bssid[4], bssid[5]);
}



void print_header(wlan_header_t *header) {
    printf("Version:       %02x\n", header->frame_control.version);
    printf("Type:          %02x %s\n", header->frame_control.type, frame_type(header));
    printf("Subtype:       %02x\n", header->frame_control.sub_type);
    printf("To DS:         %d\n", header->frame_control.to_ds);
    printf("From DS:       %d\n", header->frame_control.from_ds);
    printf("More Frags:    %d\n", header->frame_control.more_frags);
    printf("Retry:         %d\n", header->frame_control.retry);
    printf("Power Mgmt:    %d\n", header->frame_control.pwr_mgmt);
    printf("More Data:     %d\n", header->frame_control.more_data);
    printf("WEP:           %d\n", header->frame_control.wep);
    printf("Order:         %d\n", header->frame_control.order);

    print_addresses(header);
}
