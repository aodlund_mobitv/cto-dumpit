#ifndef CTO_ANDROID_BUILD_WIFI_H_CED1A979
#define CTO_ANDROID_BUILD_WIFI_H_CED1A979

int switch_channel(int channel);


struct wifi_ctx;
typedef struct wifi_ctx wifi_ctx_t;


wifi_ctx_t * init_wifi_context();
void deinit_wifi_context(wifi_ctx_t * ctx);

int start_wifi_capture(wifi_ctx_t * ctx);
const void * next_wifi_packet(wifi_ctx_t * ctx, int *len);
void stop_wifi_capture(wifi_ctx_t * ctx);

//void set_monitor_mode(wifi_ctx_t * ctx, int enable);

#endif /* end of include guard: CTO_ANDROID_BUILD_WIFI_H_CED1A979 */
