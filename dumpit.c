#include "wifi_util.h"
#include "scan.h"

#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/time.h>
#include <signal.h>
#include <stdlib.h>

typedef struct pcap_hdr_s {
        uint32_t magic_number;   /* magic number */
        uint16_t version_major;  /* major version number */
        uint16_t version_minor;  /* minor version number */
        int32_t  thiszone;       /* GMT to local correction */
        uint32_t sigfigs;        /* accuracy of timestamps */
        uint32_t snaplen;        /* max length of captured packets, in octets */
        uint32_t network;        /* data link type */
} pcap_hdr_t;

typedef struct pcaprec_hdr_s {
        uint32_t ts_sec;         /* timestamp seconds */
        uint32_t ts_usec;        /* timestamp microseconds */
        uint32_t incl_len;       /* number of octets of packet saved in file */
        uint32_t orig_len;       /* actual length of packet */
} pcaprec_hdr_t;

static int g_run_scan_thread = 1;


void siginthandler(int sig) {
    fprintf(stderr, "Interrupted by user\n");
    g_run_scan_thread = 0;
}

FILE* open_pcap_file(const char *file_name) {
    
    FILE *fp = NULL;
    fp = fopen(file_name, "wb");

    pcap_hdr_t pcap_hdr = {  0xa1b2c3d4, 2,  4, 0, 0, 65535, 105 };
    int result = fwrite(&pcap_hdr, sizeof(pcap_hdr_t), 1, fp);
    if (result == 0) {
        fprintf(stderr, "Failed to open: %s\n", file_name);
        fclose(fp);
        fp = NULL;
    } else {
        fprintf(stderr, "Writing packets to %s\n", file_name);
    }
    return fp;
}

int main(int argc, char *argv[]) {
    signal(SIGINT, siginthandler);

    wifi_ctx_t *wifi_ctx = init_wifi_context();

    if (argc < 2) {
        fprintf(stderr, "usage: %s <channel> [output file name]\n", argv[0]);
        return -1;
    }

    switch_monitor(1);
    int channel = atoi(argv[1]);
    char *file_name = "/data/local/dumpit.pcap";

    if (argc == 3) {
        file_name = argv[2];
    }
    FILE *pcap_dump = NULL;

    if (wifi_ctx) {
        pcaprec_hdr_t rec_hdr;
        wlan_header_t * wlanheader;
        struct timeval tv = {0,0};
        int len;
        int packet_count = 0;

        fprintf(stderr, "Capturing on channel %d\n", channel);
        switch_channel(channel);

        pcap_dump = open_pcap_file(file_name);
        if (pcap_dump) {

            start_wifi_capture(wifi_ctx);
            while (g_run_scan_thread) {
                    
                wlanheader  = (wlan_header_t*)next_wifi_packet(wifi_ctx, &len);
                if (wlanheader) {
                        packet_count++;
                        gettimeofday(&tv, NULL);
                        rec_hdr.ts_sec = tv.tv_sec;
                        rec_hdr.ts_usec = tv.tv_usec;
                        rec_hdr.incl_len = len;
                        rec_hdr.orig_len = len;

                        int result = fwrite(&rec_hdr, sizeof(pcaprec_hdr_t), 1, pcap_dump);
                        if (result == 0) {
                            fprintf(stderr, "Failed to write pcap header\n");
                            fclose(pcap_dump);
                            pcap_dump = 0;
                            g_run_scan_thread = 0;
                        } else {
                            result = fwrite(wlanheader, len, 1, pcap_dump);
                            if (result == 0) {
                                fprintf(stderr, "Failed to write packet\n");
                                fclose(pcap_dump);
                                pcap_dump = 0;
                                g_run_scan_thread = 0;
                            }
                        }
                }
            }
            stop_wifi_capture(wifi_ctx);
            fprintf(stderr, "packet count: %d\n", packet_count);
        }   
        if (pcap_dump)
            fclose(pcap_dump);
        deinit_wifi_context(wifi_ctx);
    } else {
        fprintf(stderr, "Failed to initialize capture context\n");
    }

    switch_monitor(1);
    return 0;
}
