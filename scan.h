#ifndef __SCAN_H__
#define __SCAN_H__

#include <stdint.h>

struct __attribute__((__packed__)) wlan_frame_control {
    unsigned int version         : 2;
    unsigned int type            : 2;
    unsigned int sub_type        : 4;
    unsigned int to_ds           : 1;
    unsigned int from_ds         : 1;
    unsigned int more_frags      : 1;
    unsigned int retry           : 1;
    unsigned int pwr_mgmt        : 1;
    unsigned int more_data       : 1;
    unsigned int wep             : 1;
    unsigned int order           : 1;
};

typedef struct wlan_frame_control wlan_frame_control_t;

struct __attribute__((__packed__)) sequence_frame_control {
    unsigned int fragment    : 4;
    unsigned int sequence    : 12;
};

typedef struct sequence_frame_control sequence_frame_control_t;

struct __attribute__((__packed__)) wlan_header {
    wlan_frame_control_t        frame_control;
    uint16_t                    duration;
    uint8_t                     addr1[6];
    uint8_t                     addr2[6];
    uint8_t                     addr3[6];
    sequence_frame_control_t    seq;
    uint8_t                     addr4[6];
};

typedef struct wlan_header wlan_header_t;


#define pletohs(p) ((unsigned short) ((unsigned short)*((const unsigned char *)(p)+1)<<8| (unsigned short)*((const unsigned char *)(p)+0)<<0))
    
#define FRAME_TYPE_MANAGEMENT       0x0     // 00
#define FRAME_TYPE_CONTROL          0x1     // 01
#define FRAME_TYPE_DATA             0x2     // 02

#define FRAME_SUB_TYPE_DATA_DATA                 0x0        // 0000
#define FRAME_SUB_TYPE_DATA_DATA_CF_ACK          0x1        // 0001
#define FRAME_SUB_TYPE_DATA_DATA_CF_POLL         0x2        // 0010
#define FRAME_SUB_TYPE_DATA_DATA_CF_ACK_POLL     0x3        // 0011
#define FRAME_SUB_TYPE_DATA_NULL                 0x4        // 0100
#define FRAME_SUB_TYPE_DATA_QOS                  0x8        // 1000
// ...

void print_header(wlan_header_t *header);
void print_addresses(wlan_header_t *header);

uint8_t * get_da(wlan_header_t *header);
uint8_t * get_sa(wlan_header_t *header);
uint8_t * get_ra(wlan_header_t *header);
uint8_t * get_ta(wlan_header_t *header);
uint8_t * get_bssid(wlan_header_t *header);

int is_data_packet(wlan_header_t *header);
int is_same_transmitter(wlan_header_t *hdr1, wlan_header_t *hdr2);
int is_broadcast(wlan_header_t *header);
int is_from_ap(wlan_header_t *header);

#endif // __SCAN_H__

